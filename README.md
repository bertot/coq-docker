These containers provide stacks leading to a variety of Coq instances

 - one with coquelicot
 - one with mathematical components (up to the odd order theorem)
 - one with smtcoq

What is slightly depressing with my current understanding of the Dockerfile format is that these files are not self sufficient.  We also need to know how they have been used.  In my case, the images have been build in the following manner:

    sudo docker build -t opam opam-container
    sudo docker build -t coq.8.6 coq-container
    sudo docker build -t coquelicot.3.0.0 coquelicot-container
    sudo docker build -t ssralgebra.1.6 ssralgebra-container
    sudo docker build -t mathcomp.1.6.1 mathcomp-container
    sudo docker build -t oddorder.1.6.1 oddorder-container
    sudo docker build -t smtcoq.8.6 smtcoq-container
    sudo docker build -t smtcoq-cvc4.8.6 smtcoq+cvc4-container

